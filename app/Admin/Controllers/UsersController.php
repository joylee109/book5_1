<?php

namespace App\Admin\Controllers;

use App\Models\User;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class UsersController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content) {
        return Admin::content(function(Content $content) {
            // 页面标题
            $content->header('用户列表');
            $content->body($this->grid());
        });
    }

    /**
     * Show interface.
     *
     * @param mixed   $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        // 根据回调函数，在页面上用表格的形式展示用户记录
        return Admin::grid(User::class,function(Grid $grid) {
            // 创建一个列名为 ID 的列，内容使用户的id 字段，并且可以在前端页面点击排序
            $grid->id('ID')->sortable();
            // 创建一个列名为 用户名的列，内容使用户的name 字段，下面的email() 和created_at() 同理
            $grid->name('用户名');
            $grid->email('邮箱');
            $grid->email_verified('已验证邮箱')->display(function($value) {
                return $value ? '是' : '否';
            });
            $grid->created_at('注册时间');
            // 不再页面显示 `新建` 按钮，因为我们不需要在后台新建用户
            $grid->disableCreateButton();
            $grid->actions(function($actions) {
                // 不再每一行后面展示查看按钮
                $actions->disableView();
                // 不再每一行后面展示删除按钮
                $actions->disableDelete();
                // 不再每一行后面展示编辑按钮
                $actions->disableEdit();
            });

            $grid->tools(function($tools) {
                // 禁用批量删除按钮
                $tools->batch(function($batch) {
                    $batch->disableDelete();
                });
            });
        });
        $grid = new Grid(new User);

    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->id('Id');
        $show->name('Name');
        $show->email('Email');
        $show->password('Password');
        $show->remember_token('Remember token');
        $show->email_verified('Email verified');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

}
