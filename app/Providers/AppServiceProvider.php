<?php

namespace App\Providers;

use Monolog\Logger;
use Yansongda\Pay\Pay;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(){
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(){
        // 往服务容器中注入一个名为 alipay 的单例对象
        $this->app->singleton('alipay',function() {
            $config = config('pay.alipay');
            // 注意：回调地址必须是完整的带有域名的 URL，不可以是相对路径。使用 route() 函数生成的 URL 默认就是带有域名的完整地址。
            $config['notify_url'] = route('payment.alipay.notify');  #支付宝后台通知
            $config['return_url'] = route('payment.alipay.return');  #前端redirect
            // 判断当前项目运行环境是否为线上环境
            if(!app()->isLocal()) {
                $config['mode'] = 'dev';
                $config['log']['level'] = Logger::WARNING;
            } else {
                $config['mode'] = 'dev';
                $config['log']['level'] = Logger::DEBUG;
            }
            // 调用Yansongda\Pay 来创建一个支付宝支付对象
            return Pay::alipay($config);
        });

        $this->app->singleton('wechat_pay',function() {
            $config = config('pay.wechat');
            $config['notify_url'] = route('payment.wechat.notify');
            if(!app()->isLocal()) {
                $config['log']['level'] = Logger::WARNING;
            } else {
                $config['log']['level'] = Logger::DEBUG;
            }
            // 调用Yansongda\Pay 来创建一个微信支付对象
            return Pay::wechat($config);
        });
    }
}
