<?php

namespace App\Providers;

use App\Http\Requests\UserAddressRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\Order;
use App\Models\UserAddress;

use App\Policies\OrderPolicy;
use App\Policies\UserAddressPolicy;

class AuthServiceProvider extends ServiceProvider
{

    protected $policies = [
        UserAddress::class      => UserAddressPolicy::class,
        Order::class            => OrderPolicy::class,
    ];


    public function boot()
    {
        $this->registerPolicies();
    }
}
