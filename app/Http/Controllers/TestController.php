<?php

namespace App\Http\Controllers;

use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use Overtrue\Socialite\SocialiteManager;

class TestController extends Controller
{
    public function index() {
        $config = [
            'qq' => [
                'client_id'         => '101526463',
                'client_secret'     => 'e968abcdf6eff3a369c99c5281d9f185',
                'redirect'          => 'https://shop.weierban.top/qq/oauth'
            ]
        ];
        $socialite = new SocialiteManager($config);
        $response = $socialite->driver('qq')->redirect();
        // echo $response;
        $response->send();
    }

    public function callback() {
        $config = [
            'qq' => [
                'client_id'         => '101526463',
                'client_secret'     => 'e968abcdf6eff3a369c99c5281d9f185',
                'redirect'          => 'https://shop.weierban.top/qq/oauth'
            ]
        ];
        $socialite = new SocialiteManager($config);
        $user = $socialite->driver('qq')->stateless()->user();
        var_dump($user);
        echo $user->getId();
        echo '<br />';
        // echo $user->getUnionid();
        echo '<br />';
        echo $user->getNickname();
        echo '<br />';
        echo $user->getName();
        echo '<br />';
        echo $user->getEmail();
        echo '<br />';
        echo $user->getProviderName();
        echo '<br />';
        echo $user->getAvatar();
        echo '<br />';
    }

}
