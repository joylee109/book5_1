<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('优惠券标题');
            $table->string('code')->unique()->comment('优惠码');
            $table->string('type')->comment('优惠券类型:固定金额 百分比');
            $table->decimal('value')->comment('折扣值');
            $table->unsignedInteger('total')->comment('优惠券数量');
            $table->unsignedInteger('used')->default(0)->comment('已兑换数量');
            $table->decimal('min_amount',10,2)->comment('优惠券最低订单金额');
            $table->datetime('not_before')->nullable()->comment('开始时间');
            $table->datetime('not_after')->nullable()->comment('结束时间');
            $table->boolean('enabled')->default(false)->comment('是否生效');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_codes');
    }
}
