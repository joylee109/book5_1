<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index()->default(0)->comment('所属用户id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('province',128)->default('')->comment('省');
            $table->string('city',128)->default('')->comment('市');
            $table->string('district',128)->default('')->comment('县');
            $table->string('address',256)->default('')->comment('具体地址');
            $table->integer('zip')->unsigned()->default(0)->comment('邮政编码');
            $table->string('contact_name',64)->default('')->comment('联系人姓名');
            $table->string('contact_phone',64)->default('')->comment('联系人电话');
            $table->dateTime('last_used_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_addresses');
    }
}
