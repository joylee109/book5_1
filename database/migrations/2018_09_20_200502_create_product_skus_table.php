<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSkusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_skus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',128)->defult('')->comment('sku 名称');
            $table->string('description',256)->defult('')->comment('sku 描述');
            $table->decimal('price',10,2)->default(0.00)->comment('sku 价格');
            $table->integer('stock')->unsigned()->default(0)->comment('库存');
            $table->integer('product_id')->unsigned()->default(0)->comment('所属商品id');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_skus');
    }
}
