<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',128)->default('')->comment('商品名称');
            $table->text('description')->comment('商品详情');
            $table->string('image')->comment('商品封面图片文件路径');
            $table->boolean('on_sale')->default(true)->comment('是否售卖 0不出售 1出售');
            $table->float('rating')->default(5)->comment('商品平均评分');
            $table->integer('sold_count')->unsigned()->default(0)->comment('商品销量');
            $table->integer('review_count')->unsigned()->default(0)->comment('评价数量');
            $table->decimal('price',10,2)->default(0.00)->comment('sku 最低价格');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
