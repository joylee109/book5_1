<?php

use Illuminate\Database\Seeder;
use App\Models\User;


class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $users = factory(User::class)->times(100)->make();
        // User::makeVisible('password')->insert($users->toArray());
        User::insert($users->makeVisible(['password'])->toArray());

    }
}
