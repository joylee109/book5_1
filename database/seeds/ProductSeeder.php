<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\ProductSku;
use App\Models\CouponCode;
class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $products = factory(Product::class)->times(30)->create();
        foreach($products as $product) {
            $skus = factory(ProductSku::class)->times(3)->make()->each(function($sku) use($product) {
                $sku->product_id = $product->id;
            });
            ProductSku::insert($skus->toArray());
            $product->update(['price' => $skus->min('price')]);
        }
    }
}
