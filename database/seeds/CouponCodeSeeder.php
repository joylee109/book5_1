<?php

use Illuminate\Database\Seeder;
use App\Models\CouponCode;

class CouponCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $couponCodes = factory(CouponCode::class)->times(10)->make();
        CouponCode::insert($couponCodes->toArray());
    }
}
