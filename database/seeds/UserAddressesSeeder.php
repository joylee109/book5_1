<?php

use App\Models\User;
use App\Models\UserAddress;
use Illuminate\Database\Seeder;

class UserAddressesSeeder extends Seeder
{
    public function run()
    {
        User::all()->each(function(User $user) {
            $userAddresses = factory(UserAddress::class)->times(random_int(1,3))->make()->each(function($userAddress) use($user) {
                $userAddress->user_id = $user->id;
            });

            UserAddress::insert($userAddresses->toArray());
        });
    }
}
