<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run() {
        // $this->call(UsersTableSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(CouponCodeSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(UserAddressesSeeder::class);
        $this->call(OrdersSeeder::class);
    }
}
