<?php

// Route::get('test','TestController@index')->name('test.index');
Route::get('/', 'PagesController@root')->name('root');
Route::redirect('/','/products')->name('root');
Route::get('products','ProductsController@index')->name('products.index');
Route::get('/qq','TestController@index')->name('test.index');
Route::get('/qq/oauth','TestController@callback')->name('test.qqcallback');

Auth::routes();
Route::group(['middleware' => 'auth'],function(){
    Route::get('/email_verify_notice','PagesController@emailVerifyNotice')->name('email_verify_notice');
    Route::get('/email_verification/verify','EmailVerificationController@verify')->name('email_verification.verify');
    Route::get('/email_verification/send','EmailVerificationController@send')->name('email_verification.send');

    Route::group(['middleware' => 'email_verified'],function() {
        Route::get('user_addresses','UserAddressesController@index')->name('user_addresses.index');
        Route::get('user_addresses/create','UserAddressesController@create')->name('user_addresses.create');
        Route::post('user_addresses','UserAddressesController@store')->name('user_addresses.store');
        Route::get('user_addresses/{user_address}','UserAddressesController@edit')->name('user_addresses.edit');
        Route::put('user_addresses/{user_address}','UserAddressesController@update')->name('user_addresses.update');
        Route::delete('user_addresses/{user_address}','UserAddressesController@destroy')->name('user_addresses.destroy');
        // 收藏商品
        Route::post('products/{product}/favorite','ProductsController@favor')->name('products.favor');
        // 取消收藏商品
        Route::delete('products/{product}/favorite','ProductsController@disfavor')->name('products.disfavor');
        // 收藏商品列表
        Route::get('products/favorites','ProductsController@favorites')->name('products.favorites');
        // 加入购物车
        Route::post('cart','CartController@add')->name('cart.add');
        // 购物车列表
        Route::get('cart','CartController@index')->name('cart.index');
        // 删除购物车
        Route::delete('cart/{sku}','CartController@remove')->name('cart.remove');
        // 加入订单
        Route::post('orders','OrdersController@store')->name('orders.store');
        // 订单列表
        Route::get('orders','OrdersController@index')->name('orders.index');
        // 订单详情
        Route::get('orders/{order}','OrdersController@show')->name('orders.show');
        // 支付宝支付
        Route::get('payment/{order}/alipay','PaymentController@payByAlipay')->name('payment.alipay');
        // 支付宝支付跳转
        Route::get('payment/alipay/return','PaymentController@alipayReturn')->name('payment.alipay.return');
        // 微信支付
        Route::get('payment/{order}/wechat','PaymentController@payByWechat')->name('payment.wechat');
        // 确认收货
        Route::post('orders/{order}/received','OrdersController@received')->name('orders.received');
        // 订单评价页面
        Route::get('orders/{order}/review','OrdersController@review')->name('orders.review.show');
        // 订单评价逻辑
        Route::post('orders/{order}/review','OrdersController@sendReview')->name('orders.review.store');
        // 申请退款
        Route::post('orders/{order}/apply_refund','OrdersController@applyRefund')->name('orders.apply_refund');
        // 优惠券显示
        Route::get('coupon_codes/{code}','CouponCodesController@show')->name('coupon_codes.show');
    });

});
Route::get('products/{product}','ProductsController@show')->name('products.show');
// 支付宝支付通知
Route::post('payment/alipay/notify','PaymentController@alipayNotify')->name('payment.alipay.notify');
// 微信支付通知
Route::post('payment/wechat/notify','PaymentController@wechatNotify')->name('payment.wechat.notify');
// 微信退款通知
Route::post('payment/wechat/refund_notify','PaymentController@wechatRefundNotify')->name('payment.wechat.refund_notify');

